<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>tugas 12</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
        <label> First name:</label> <br><br>
        <input type="text" name="fname"> <br><br>
        <label> Last name:</label> <br><br>
        <input type="text" name="lname"><br><br>
        <label> Gender:</label> <br><br>
        <input type="radio" name="Gender" id="" value="1"> Male <br>
        <input type="radio" name="Gender" id="" value="2"> Female <br>
        <input type="radio" name="Gender" id="" value="3"> Other <br><br>
        <label> Nationality:</label> <br><br>
        <select name="Nationality" id="">
            <option value="1">Indonesian</option>
            <option value="2">English</option>
            <option value="3">Singapore</option>
        </select><br><br>
        <label> Language Spoken:</label> <br><br>
        <input type="checkbox" name="Language Spoken" value="1">Bahasa Indonesia<br>
        <input type="checkbox" name="Language Spoken" value="2">English<br>
        <input type="checkbox" name="Language Spoken" value="3">Other<br><br>
        <label> Bio:</label> <br><br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea><br>

        <input type="submit" value="Kirim">
    </form>
</body>
</html>